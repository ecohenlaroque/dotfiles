function install_starship
  mkdir -p "$HOME/.local/bin"
  curl -fsSL https://starship.rs/install.sh | bash -s -- -y -b "$HOME/.local/bin"
end

