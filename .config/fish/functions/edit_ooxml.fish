function edit_ooxml
  if test (count $argv) -lt 1
    echo "error: expected filename"
    return 1
  end

  set tmpdir (mktemp /tmp/ooxml.XXXXXX)
  rm $tmpdir
  mkdir -p $tmpdir

  set dirname (dirname $argv[1])
  set filename (basename $argv[1])
  cp "$argv[1]" $tmpdir
  cd $tmpdir
  unzip $filename
  rm $filename

  $EDITOR
  if test $status -ne 0
    cd -
    rm -rf $tmpdir
    return 1
  end

  zip $filename (find . -type f)
  mv "$tmpdir/$filename" "$dirname/new-$filename"
  cd -
  rm -rf $tmpdir
end

