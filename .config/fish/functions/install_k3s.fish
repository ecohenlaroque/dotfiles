function install_k3s
  getopts $argv | while read -l key value
    switch $key
      case u url
        set url $value
      case t token
        set token $value
    end
  end
  curl -sfL https://get.k3s.io | env K3S_URL=$url K3S_TOKEN=$token sh -
end
