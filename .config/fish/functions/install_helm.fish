function install_helm
  switch (uname -s)
    case Linux
      set url "https://get.helm.sh/helm-v2.13.0-linux-amd64.tar.gz"
      set arch linux
    case Darwin
      set url "https://get.helm.sh/helm-v2.13.0-darwin-amd64.tar.gz"
      set arch darwin
  end

  curl -L $url > ~/Downloads/helm.tar.gz
  cd ~/Downloads/
  tar -xvf helm.tar.gz
  mv ~/Downloads/$arch-amd64/helm /usr/local/bin/helm
  cd -
end
