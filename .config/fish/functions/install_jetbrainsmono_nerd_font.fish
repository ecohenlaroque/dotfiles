function install_jetbrainsmono_nerd_font
  set distro (uname -s)
  if test "$distro" != "Linux"
    echo This install script only works for Linux!
    return 1
  end
  mkdir -p ~/.local/share/fonts/
  curl -sLo ~/.local/share/fonts/JetBrainsMono.zip https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/JetBrainsMono.zip
  cd ~/.local/share/fonts/
  unzip JetBrainsMono.zip
  cd -
end
