function install_fish_nightly_appimg
  mkdir -p ~/.local/bin/
  curl -Lo ~/.local/bin/nfish https://download.opensuse.org/repositories/shells:/fish:/nightly:/master/AppImage/fish-3.1b1+88.gc6f85238b-lp151.1.1.Build1.1.glibc2.25-x86_64.AppImage
  chmod +x ~/.local/bin/nfish
end
