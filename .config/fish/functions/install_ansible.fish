function install_ansible
    if not command -sq python
        echo error: python not found. exiting
        return 1
    end
    if not command -sq pip
        curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
        python get-pip.py --user
    end
    if not pip show ansible > /dev/null
        pip install --user ansible
    end
end

