function install_lazygit
    set os (uname -s)
    set arch (uname -m)
    if test "$arch" = "aarch64"
        set arch "armv6"
    end
    set filename lazygit_0.13_(echo $os)_(echo $arch).tar.gz
    mkdir -p ~/.local/bin/
    curl -Lo ~/.local/bin/$filename https://github.com/jesseduffield/lazygit/releases/download/v0.13/$filename
    cd ~/.local/bin/
    tar -xvf $filename
    cd -
end

