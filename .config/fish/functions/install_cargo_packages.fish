function install_cargo_packages
  set pkgs bat \
    bliss \
    btm \
    cargo-add \
    cargo-bundle \
    cargo-expand \
    cargo-tree \
    cargo-watch \
    cargo-workspaces \
    fselect \
    gitui \
    sd \
    tokei \
    tealdeer

  for p in $pkgs
    if ! command -sq "$p"
      echo "Installing $p..."
      cargo install "$p"
    else
      echo "$p already installed!"
    end
  end
end
