function kubens
  if ! command -sq kubectl
    echo "error: kubectl not found"
    exit 1
  end
  if ! command -sq getopts
    echo "error: getopts not found"
    exit 1
  end


  if test (count $argv) -eq 0
    set new_ns (kubectl get namespaces -o json | jq -r '.items[].metadata.name' | fzf)
    if test $status -eq 0
      _kubens_switch $new_ns
    end
  else
    getopts $argv | while read -l key value
      switch $key
        case c current
          set cc (kubectl config current-context)
          kubectl config view -o json | jq -r ".contexts[] | select(.name == \"$cc\") | .context.namespace"
        case h help
          _kubens_help
        case _
          if test "$value" = "-"
            if test "$_kubens_fish_previous_namespace" != ""
              _kubens_switch $_kubens_fish_previous_namespace
            else
              echo "error: no previous namespace to switch to"
            end
          else
            _kubens_switch $value
          end
      end
    end
  end
end

function _kubens_help
  echo "USAGE:"
  echo "  kubens                    : list the namespaces in the current context"
  echo "  kubens <NAME>             : change the active namespace of current context"
  echo "  kubens -                  : switch to the previous namespace in this context"
  echo "  kubens -c, --current      : show the current namespace"
  echo "  kubens -h,--help          : show this message"
end

function _kubens_switch
  set cc (kubectl config current-context)
  set ns (kubectl config view -o json | jq -r ".contexts[] | select(.name == \"$cc\") | .context.namespace")
  set -g _kubens_fish_previous_namespace $ns
  kubectl config set-context (kubectl config current-context) --namespace $argv[1] > /dev/null
  echo "switched to `$argv[1]`"
end
