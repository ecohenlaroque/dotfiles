function delete_vim_data
  rm -rf ~/.config/coc/extensions/
  rm -rf ~/.local/share/nvim/plugged/
  rm -rf ~/.local/share/nvim/site/autoload/plug.vim
  rm -rf ~/.vim/plugged/
end
