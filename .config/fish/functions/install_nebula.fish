function install_nebula
    switch (uname -s)
        case "Linux"
            curl -Lo /tmp/nebula.tgz https://github.com/slackhq/nebula/releases/download/v1.1.0/nebula-linux-amd64.tar.gz
            sudo tar -xvf /tmp/nebula.tgz -C /usr/bin
            rm -f /tmp/nebula.tgz
        case "*"
            echo Use a package manager instead!
            return 1
    end
end

