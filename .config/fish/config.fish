if not functions -q fisher
  set -q XDG_CONFIG_HOME; or set XDG_CONFIG_HOME $HOME/.config
  curl https://git.io/fisher --create-dirs -sLo $XDG_CONFIG_HOME/fish/functions/fisher.fish
  fish -c fisher
end

if status is-interactive; and not set -q TMUX; and command -sq tmux
  if tmux list-sessions >/dev/null 2>&1
    tmux -2 attach
  else
    tmux -2 new
  end
end

abbr -a a "ansible"
abbr -a ag "ansible-galaxy"
abbr -a ap "ansible-playbook"
abbr -a av "ansible-vault"
abbr -a code "env TMUX=\"\" code"
abbr -a codei "env TMUX=\"\" code-insiders"
abbr -a configed "$EDITOR ~/.config/fish/config.fish; and source ~/.config/fish/config.fish"
abbr -a cs "source ~/.config/fish/config.fish"
abbr -a novim "nvim --noplugin"
abbr -a nvims "nvim -S Session.vim"
abbr -a t1 "tail -fn 1000"

abbr -a d "docker"
abbr -a dc "docker-compose"
abbr -a ds "docker stats"

abbr -a g "git"
abbr -a gb "git branch"
abbr -a gco "git checkout"
abbr -a gfco "git checkout (git branch --all | sed 's/^\*\{0,1\}//g' | awk '{print \$1}' | fzf)"
abbr -a gl "git l"
abbr -a gm "git merge"
abbr -a gmt "git mergetool"
abbr -a gs "git s"
abbr -a lg "lazygit"

abbr -a kk "k3s kubectl"
abbr -a kx "kubectx"
abbr -a kn "kubens"
abbr -a mon "tmux set mouse on"
abbr -a moff "tmux set mouse off"
abbr -a tclear "clear; and tmux clear-history"
abbr -a tsn "node -r ts-node/register"
abbr -a ts-node "node -r ts-node/register"
abbr -a y "yadm"
abbr -a yap "yadm add --patch"
abbr -a yds "yadm diff --staged"
abbr -a wx "curl wttr.in"

abbr -a ldl "tail -f /var/log/system.log"
abbr -a lct "launchctl"
abbr -a lctu "launchctl unload"
abbr -a lctl "launchctl load"
abbr -a slct "sudo launchctl"
abbr -a slctu "sudo launchctl unload"
abbr -a slctl "sudo launchctl load"

abbr -a sct "sudo systemctl"
abbr -a sctdr "sudo systemctl daemon-reload"
abbr -a jct "sudo journalctl"

abbr -a suct "sudo supervisorctl"

command -sq lsof; and abbr -a listening "lsof -P -iTCP -sTCP:LISTEN"

set -gx EDITOR nvim
command -sq rg; and set -gx FZF_DEFAULT_COMMAND "rg --files"
set -gx JOBS 13
set -gx SPACEFISH_PROMPT_ADD_NEWLINE false
set -gx SPACEFISH_PACKAGE_SHOW false
set -gx WASMTIME_HOME "$HOME/.wasmtime"

# PATH
for p in /snap/bin $HOME/go/bin $HOME/.local/bin $HOME/.cargo/bin $WASMTIME_HOME/bin
  if [ -d $p ]
    string match -r "$p" "$PATH" > /dev/null; or set -gx PATH $p $PATH
  end
end

set BASE16_SHELL "$HOME/.config/base16-shell/"
if [ -d $BASE16_SHELL ]
  if status --is-interactive
    source "$BASE16_SHELL/profile_helper.fish"
  end
end

if [ -d /home/linuxbrew/.linuxbrew/ ]
  set -gx HOMEBREW_PREFIX "/home/linuxbrew/.linuxbrew";
  set -gx HOMEBREW_CELLAR "/home/linuxbrew/.linuxbrew/Cellar";
  set -gx HOMEBREW_REPOSITORY "/home/linuxbrew/.linuxbrew/Homebrew";
  set -g fish_user_paths "/home/linuxbrew/.linuxbrew/bin" "/home/linuxbrew/.linuxbrew/sbin" $fish_user_paths;
  set -q MANPATH; or set MANPATH ''; set -gx MANPATH "/home/linuxbrew/.linuxbrew/share/man" $MANPATH;
  set -q INFOPATH; or set INFOPATH ''; set -gx INFOPATH "/home/linuxbrew/.linuxbrew/share/info" $INFOPATH;
end

# LD_LIBRARY_PATH
if [ -d $HOME/.local/lib ]
  set -gx LD_LIBRARY_PATH $HOME/.local/lib $LD_LIBRARY_PATH
end

# VI-mode (hybrid):
set -g fish_key_bindings fish_hybrid_key_bindings
bind -M insert \cy 'yadm ls | fzf | awk "{ print \"$HOME/\" \$0 }" | xargs $EDITOR'

if command -sq exa
  abbr -a ls "exa --git"
  abbr -a ll "exa -al --git"
else if command -sq lsd
  abbr -a ls "lsd"
  abbr -a ll "lsd -Al"
end
command -sq starship; and eval (starship init fish)
command -sq direnv; and direnv hook fish | source
