#!/bin/bash

os=$(uname -s)
if [[ "$os" = "Darwin" ]]; then
  #
  # macOS
  #
  command -v brew >/dev/null 2>&1 ||{
    # Install Homebrew:
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
  }
  brew install git fish neovim tmux yadm

  echo "installed: brew, fish, git, neovim, tmux yadm"
elif [[ "$os" = "Linux" ]]; then
  #
  # Linux
  #
  export PATH="$HOME/.cargo/bin:$PATH"

  sudo apt-get update
  sudo DEBIAN_FRONTEND=noninteractive apt-get install -y curl git software-properties-common tmux wget yadm

  sudo apt-add-repository ppa:fish-shell/release-3
  sudo add-apt-repository ppa:neovim-ppa/stable
  sudo apt-get update
  sudo apt-get install -y fish neovim
  echo "installed: curl, fish, git, neovim, tmux yadm"
else
  echo "Unsupported operating system $os"
  exit 1
fi

if ! grep fish /etc/shells >/dev/null; then
  echo "Adding fish to /etc/shells..."
  which fish | sudo tee -a /etc/shells
  chsh -s $(which fish)
fi

if ! test -d ~/.config/yadm/; then
  yadm clone https://gitlab.com/jrop/dotfiles
fi
